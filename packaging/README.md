# Packaging

Packaging is the is the set of scripts that package Mezuro projects for distribution.

## Contributing

Please, have a look the wiki pages about development workflow and code standards:

* https://gitlab.com/mezuro/mezuro/wikis/Development-workflow
* https://gitlab.com/mezuro/mezuro/wikis/Standards

## Installation

`bundle install`

## Usage

We use rake as our CLI interface. Thus for a complete list of available tasks please run `rake -T`.

Your routine usage should be:

```bash
rake debian:all
rake debian:publish[kalibro_configurations]
rake debian:publish[kalibro_processor]
rake debian:publish[prezento]
rake debian:publish[prezento_nginx]

rake centos:all
rake centos:publish[kalibro_configurations]
rake centos:publish[kalibro_processor]
rake centos:publish[prezento]
rake centos:publish[prezento_nginx]
```
