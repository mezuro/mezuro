# Revision history for Kolekti Analizo

The Kolekti Analizo gem implements a collector for C, C++ and Java using
Analizo with Kolekti

## Unreleased

## v5.0.4 - 18/03/2018

* No changes. Other projects have changed

## v5.0.3 - 13/03/2018

* No changes. Other projects have changed

## v5.0.2 - 12/03/2018

* No changes. Other projects have changed

## v5.0.1 - 11/03/2018

* No changes. Other projects have changed

## v5.0.0 - 05/03/2018

* Fix unknown metric default value fetching error type (Using UnavailableMetricError)
* Update to Ruby 2.4.1
* Restrict codeclimate-test-reporter version
* Development on Mezuro's single repository

## v0.0.5 - 15/04/2016

* Add proper cucumber integration
* Fix RSpec integration
* Add travis script
* Update kolekti

## v0.0.4 - 23/03/2016

* Use symbols instead of strings on supported_metrics code

## v0.0.2 / v0.0.3 - 01/03/2016

* Adapt kolekti_analizo to the new Kolekti API

## v0.0.1 - 19/02/2016

* Initial release

---

Kolekti Analizo. Copyright (C) 2016-2018 The Mezuro Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <http://www.gnu.org/licenses/>.

